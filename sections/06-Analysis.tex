% !TEX root =  ../Main.tex

\section{Analysis}
\subsection{Ablation study}
\label{s06-1}

To analyze where the improvements of \ac{MIKe} come from, we conduct an ablation study.
Table~\ref{ablation_result} shows the results on the \ac{WoW} dataset; qualitatively similar results were observed also for the Holl-E dataset.
We consider four settings:
\begin{enumerate*}
\item No \ac{ISLe}~(\ac{MIKe}-\ac{ISLe} in Table~\ref{ablation_result}); %i.e., initiative discriminator is only jointly supervised by the learning objectives of \ac{KS} and response generation in Eq.~\ref{ks_and_g}; 
\item No initiative discriminator (\ac{MIKe}-\ac{ISLe}-ID in Table~\ref{ablation_result}); %i.e., the final distribution $P(\mathcal{K}_{\tau})$ in Eq.~\ref{fd} is defined as: $P(\mathcal{K}_{\tau})=$ $\frac{1}{2}P(\mathcal{K}_{\tau} {\mid} user)+\frac{1}{2}P(\mathcal{K}_{\tau} {\mid} sys)$;
\item No user-initiative selector (\ac{MIKe}-\ac{ISLe}-ID-UIS in Table~\ref{ablation_result});% i.e., knowledge is chosen only based on $P(\mathcal{K}_{\tau} {\mid} sys)$ in Eq.~\ref{sys_s};
\item No system-initiative selector (\ac{MIKe}-\ac{ISLe}-ID-SIS in Table~\ref{ablation_result}).% i,e., knowledge is chosen only based on $P(\mathcal{K}_{\tau} {\mid} user)$ in Eq.~\ref{user_s}.
\end{enumerate*}

The results show that all components are beneficial for \ac{MIKe} because removing any of them will decrease the results.
Without \ac{ISLe}, the performance of \ac{MIKe} falls greatly in terms of all metrics.
Concretely, it drops 0.89\% and 1.03\% in terms of R@1 on Test seen and Test Unseen, separately, indicating that the training signals only provided by \ac{KS} and generation losses are not sufficient to discriminate the initiative type of \ac{KS}, and thus it's necessary to design \ac{ISLe}.
%
Without initiative discriminator, the performance of \ac{MIKe} further goes down a lot in terms of all metrics compared to the case without \ac{ISLe}, dropping by 0.94\% and 1.09\% in terms of R@1 on Test Seen and Test Unseen, respectively, which means that explicitly distinguishing the initiative type of \ac{KS} is effective.
%
Without either user-initiative or system-initiative selector, the performance drops dramatically to the level of \ac{TMemNet}+BERT, indicating that user-initiative and system-initiative \ac{KS} have their own roles and should be coordinated to work together.

\subsection{Initiative discrimination evaluation}
\label{s06-2}


To verify whether \ac{ISLe} helps \ac{MIKe} to discriminate the initiative type of \ac{KS} effectively, we again hire annotators on Amazon Mechanical Turk to manually annotate the initiative type of \ac{KS}, and then regard the manual annotation as ground truth to evaluate the performance of the initiative discrimination of \ac{MIKe}.

As for the collection of annotation, we randomly sample 1,000 examples from the two test sets of the \ac{WoW} dataset, respectively (1,000 examples is almost one fifth of the two test sets), and each example is annotated by an annotator.
Given an example containing a context, the ground-truth chosen knowledge and the corresponding response, the annotator needs to distinguish whether the \ac{KS} in the given example is user-initiative or system-initiative.
Table~\ref{statistics} shows the statistics about the manual annotation on the two test sets, where we found the initiative type of \ac{KS} is skewed towards system-initiative \ac{KS}.

We evaluate the initiative discrimination of \ac{MIKe}, \ac{MIKe}-\ac{ISLe}, and a heuristic method with Macro-F1 score and F1 scores for these two initiative types of \ac{KS}.
For the first two methods, a current \ac{KS} is classified into user-initiative \ac{KS} if $P(u_\tau)$ in Eq.~\ref{id} is greater than 0.5 and system-initiative \ac{KS} otherwise.
For the heuristic method, a current \ac{KS} is classified as user-initiative \ac{KS} if the current user utterance contains a question mark or begins with a question word, such as ``how'', ``why'', ``who'', ``where'', ``what'' or ``when'', and as system-initiative \ac{KS} otherwise.

\begin{table}[t]
\centering
%\setlength\tabcolsep{1.1pt}
\caption{
The evaluation results of initiative discrimination on the \ac{WoW} dataset.
M-F1 denotes Macro F1 percentage. U-F1 and S-F1 denote the F1 percentages for user-initiative and system-initiative \ac{KS}, respectively.
}
\label{initiative_discrimination}
%\resizebox{0.5\textwidth}{!}{
\begin{tabular}{l ccc ccc}
\toprule
\multirow{2}{*}{\textbf{Methods}} & \multicolumn{3}{c}{\textbf{Test Seen (\%)}} & \multicolumn{3}{c}{\textbf{Test Unseen (\%)}}\\
\cmidrule(lr){2-4} \cmidrule(lr){5-7}  
& M-F1  & U-F1 & S-F1 & M-F1 & U-F1 & S-F1\\ 
\midrule
\ac{MIKe} & \textbf{62.87}  & \textbf{61.79}  & \textbf{63.95}  & \textbf{61.79}   &\textbf{61.10} & \textbf{62.48}\\
\ac{MIKe}-\ac{ISLe} &51.04  &60.59  & 41.49  & 47.29  & 60.89 & 33.69 \\
Heuristic & 51.74 &48.16  &55.31   &52.69   &49.52 & 55.86 \\
\midrule
\end{tabular}
%}
\end{table}

As shown in Table~\ref{initiative_discrimination}, \ac{MIKe} markedly outperforms others in terms of all metrics on both.
Specifically, \ac{MIKe} exceeds \ac{MIKe}-\ac{ISLe} by 11.83\% and 14.50\% on Test Seen and Test Unseen in terms of Macro-F1, respectively, indicating that \ac{ISLe} is very effective in helping \ac{MIKe} to distinguish between the two initiative types of \ac{KS}.
Though the heuristic method outperforms \ac{MIKe}-\ac{ISLe} in some cases, we found there are two common situations that the heuristic method cannot handle but \ac{MIKe} can.
First, the current user utterance usually contains implicit information needs without any question words or question mark, such as ``Let's talk about\ldots'' and ``I would like to know\ldots'', and thus the current \ac{KS} is user-initiative.
Second, the current user utterance can also contains a simple question that does not need a knowledge-grounded respons, such as ``really?'' and ``did you enjoy it?''.
In this case, the current \ac{KS} is still system-initiative, and the corresponding response should directly answer the simple question at first and then incorporates the chosen knowledge, suggesting a new conversational direction.

\begin{table*}[thb]
\centering
\small
\caption{
Case study. We marked the chosen piece of knowledge in parentheses before each response and the system utterance in the context. Note that {\color{green} \cmark} and {\color{red} \xmark} denote that the chosen knowledge is true or false, respectively. 
}
\label{case_study}
\resizebox{1\textwidth}{!}{
\begin{tabular}{m{2.0cm}m{8cm}m{8cm}}
\toprule
\textbf{} & \textbf{Example 1~(Test seen)}  & \textbf{Example 2~(Test unseen)}  \\
\midrule
\multirow{8}{*}{\textbf{Knowledge pool}} 
 & $K_{1}$: no knowledge used .  & $K_{1}$: no knowledge used . \\ 
 & $K_{2}$: while basketball is most often played as a team sport with five players on each side , two-on-two , and one-on-one competitions are also common . & $K_{2}$: instagram is a mobile , desktop , and internet - based photo - sharing application and service that allows users to share pictures and videos either publicly , or privately to pre-approved followers . \\
 & $K_{3}$: basketball is a limited contact sport played on a rectangular court .  & $K_{3}$: google photos is a photo sharing and storage service developed by google . \\
 & $K_{4}$: jordan played 15 seasons in the nba for the chicago bulls and washington wizards . & $K_{4}$: instagram is owned by facebook . \\
 & \ldots & \ldots  \\
\midrule
\multirow{4}{*}{\textbf{Context}} 
& User: are you a basketball fan ?
& User:  i hate to admit it but i spend way too much time on instagram !  \\
& System: ($K_{2}$)~yes , i am a fan of the five player sport . are you ?
& System: ($K_{2}$)~i use it for sharing photos and videos \\
& User: not as much as i used to be . i watched the bulls in the 90s when they were the dream team .~{\color{red} (the current user utterance)}
& User: do you have a lot of followers ?~{\color{red} (the current user utterance)}  \\
\midrule
\multirow{1}{*}{\textbf{Initiative type}} 
 & User-initiative \ac{KS} & System-initiative \ac{KS}\\
\midrule
\multirow{7}{*}{\textbf{Response}} 
 & DiffKS + BERT: ($K_{3}$~{\color{red} \xmark})~i do know that basketball is a limited contact sport played on a rectangular court .  & DiffKS + BERT: ($K_{3}$~{\color{red} \xmark})~i have a google \\
  & DukeNet: ($K_{2}$~{\color{red} \xmark})~i agree . i like to play basketball . i like the sport with five players on each side .
  & DukeNet: ($K_{1}$~{\color{red} \xmark})~i have a lot of followers .\\
 & SKT+PIPM+KDBTS: ($K_{2}$~{\color{red} \xmark})~i ' m not sure but i know that while basketball is most played as a team sport with five players .
 & SKT+PIPM+KDBTS: ($K_{1}$~{\color{red} \xmark})~i have not i have not .\\

 & MIKe: ($K_{4}$~{\color{green} \cmark})~i know that jordan played 15 seasons in the nba for the chicago bulls and washington wizards .  & MIKe: ($K_{4}$ ~{\color{green} \cmark})~i have a lot of followers and i do know that it is owned by facebook .\\
\bottomrule  
\end{tabular}}
\end{table*}

\subsection{Case study}
\label{s06-3}

We randomly select two examples from the \ac{WoW} test set to compare the performance of \ac{MIKe}, \ac{SKT}+\ac{PIPM}+\ac{KDBTS}, \ac{DukeNet} and \ac{DiffKS}+BERT in Table~\ref{case_study}.
%
We see that \ac{MIKe} chooses more appropriate knowledge and hence generates more engaging responses with the help of its distinctions between the two initiative types of \ac{KS}.
%
For instance, in Example~1, given the current user utterance implicitly suggesting a new topic about ``bulls/dream team'', \ac{MIKe} identifies the current \ac{KS} as user-initiative \ac{KS} and then select a piece of knowledge about ``Jordan''.
In contrast, the baselines all ignore the implicit topic drive by the user and continue to push the current \ac{KS} based on the previously selected knowledge.
%
In Example~2, given the current user utterance containing a question ``do you have a lot of followers?'' that cannot be answered with knowledge, \ac{MIKe} identifies the current \ac{KS} as system-initiative \ac{KS} and then selects a piece of knowledge about the owner of Instagram, based on the previously selected knowledge about the definition of Instagram.
The part ``i have a lot of followers'' in \ac{MIKe}'s generated response answers the simple question at first and the part ``i do know that it is owned by facebook'' incorporates the chosen knowledge.
No baseline handles this case well.
Specifically, \ac{SKT}+\ac{PIPM}+\ac{KDBTS} ignores the previously selected knowledge, but cannot find an appropriate piece of knowledge to answer the question, generating uninformative responses.
%
These baselines cannot distinguish between the two initiative types of \ac{KS}, and so they cannot know which (the current user utterance or previously selected knowledge) is the more important feature for the current \ac{KS}; misled by unimportant features, their performance on \ac{KS} suffers.
