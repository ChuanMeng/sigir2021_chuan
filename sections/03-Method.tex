% !TEX root =  ../Main.tex

\section{Methodology}
\label{03-model}

\subsection{Task formulation}
Suppose that we have a conversation $\mathcal{C}=\{(X_\tau,Y_\tau)\}^{|\mathcal{C}|}_{\tau=1}$ with $|\mathcal{C}|$ turns, where $X_\tau$ and $Y_\tau$ are the utterances produced by a user and a system at turn $\tau$, separately.
Each turn is accompanied with a knowledge pool $\mathcal{K}_{\tau}=\{K_{\tau,1},\ldots,K_{\tau,i},\ldots, K_{\tau,|\mathcal{K}_{\tau}|}\}$ (See \S\ref{s03-4-1} to know the source of knowledge), with $|\mathcal{K}_{\tau}|$ pieces of knowledge.
At turn $\tau$, given the current user utterance $X_\tau$, the previously selected knowledge $\{K_{1,sel},\ldots,K_{\tau-1,sel}\}$~(also written as $\{K_{i,sel}\}^{\tau-1}_{i=1}$) and the knowledge pool $\mathcal{K}_{\tau}$, our target is to select a piece of knowledge ${K_{\tau,sel}}$ from $\mathcal{K}_{\tau}$ and then leverage ${K_{\tau,sel}}$ to generate the response $Y_{\tau}=(y_{\tau,1},y_{\tau,2},\ldots,y_{\tau,|Y_{\tau}|})$ with $|Y_{\tau}|$ tokens.

\subsection{Overview of \ac{MIKe}}

As depicted in Fig.~\ref{figure2}, \ac{MIKe} consists of three layers: 
\begin{enumerate*}
\item an \emph{encoding layer}, 
\item a \emph{mixed-initiative knowledge selection layer}, and
\item a \emph{decoding layer}.
\end{enumerate*} 
The encoding layer uses a BERT encoder to encode the current user utterance $X_\tau$ and the knowledge pool $\mathcal{K}_{\tau}$ into latent representations.
The mixed-initiative knowledge selection layer contains a \emph{user-initiative selector}, a \emph{system-initiative selector} and an \emph{initiative discriminator}.
The user-initiative selector predicts the distribution $P(\mathcal{K}_{\tau} {\mid} user)$ over the knowledge pool $\mathcal{K}_{\tau}$ given the current user utterance $X_\tau$, while the system-initiative selector predicts the distribution $P(\mathcal{K}_{\tau}{\mid} sys)$ over the knowledge pool $K_{\tau}$ given the previously selected knowledge $\{K_{i,sel}\}^{\tau-1}_{i=1}$.
The initiative discriminator discriminates the current initiative type of \ac{KS} by estimating the probability of user-initiative \ac{KS} $P(u_\tau)\in[0,1]$ given the current user utterance $X_\tau$ and the previously selected knowledge $\{K_{i,sel}\}^{\tau-1}_{i=1}$.
Based on $P(u_\tau)$, the distributions estimated by both selectors are combined to obtain the distribution $P(\mathcal{K}_{\tau})=P(u_\tau)P(\mathcal{K}_{\tau} {\mid} user)+(1-P(u_\tau))P(\mathcal{K}_{\tau} {\mid} sys)$, from which we select the piece of knowledge ${K_{\tau,sel}}$.
The \emph{decoding layer} contains a transformer decoder to generate the response $Y_{\tau}$ given ${K_{\tau,sel}}$ and $X_\tau$.

We refer to the initiative discriminator defined above as the \emph{student initiative discriminator} and also introduce a \emph{teacher initiative discriminator}.
During training, \ac{ISLe} supervises \ac{MIKe} to discriminate the initiative type of \ac{KS} via two tasks:
\begin{enumerate*}
\item \emph{locating missing knowledge}; given all pieces of ground-truth chosen knowledge in a conversation, a piece of knowledge at one of the turns is randomly deleted and then the teacher is required to locate missing knowledge (detecting the knowledge closely after the missing knowledge); and   
\item \emph{learning with pseudo initiative labels}; at each turn, the teacher generates a pseudo-label indicating the initiative type of \ac{KS}; then, the student is required to learn the pseudo-label estimated by the teacher via \ac{MSE} loss.
\end{enumerate*}
During inference, only the student initiative discriminator is run.

\subsection{Encoding layer}
Given the current user utterance $X_\tau$, we encode it into latent representation $\mathbf{H}^{X_{\tau}}$ via $\mathrm{BERT}$~\citep{devlin2019bert}, and then convert it into the condensed representation $\mathbf{h}^{X_{\tau}}$ via an average pooling operation~{\citep{cer2018universal}}:
\begin{equation}
\label{enc}
\mathbf{H}^{X_\tau}=\mathrm{BERT}(X_{\tau})\in \mathbb{R}^{|X_{\tau}| \times d},\
\mathbf{h}^{X_{\tau}}=\mathrm{pooling}(\mathbf{H}^{X_\tau})\in \mathbb{R}^{1\times d},
\end{equation}
where $d$ denotes the hidden size.
Likewise, given the knowledge pool $\smash{\mathcal{K}_{\tau}=\{K_{\tau,1},\ldots,K_{\tau,i},\ldots, K_{\tau,|\mathcal{K}_{\tau}|}\}}$, we get the representations of each piece of knowledge,  $\mathbf{H}^{K_{\tau,i}}\in\mathbb{R}^{|K_{\tau,i}| \times d}$ and $\mathbf{h}^{K_{\tau,i}}\in\mathbb{R}^{1 \times d}$.
We also track the previously selected knowledge representations $\{\mathbf{h}^{K_{i,sel}}\}^{\tau-1}_{i=1}\in\mathbb{R}^{(\tau-1) \times d}$.
 
\subsection{Mixed-initiative knowledge selection layer}
\label{s03-4}
\subsubsection{User-initiative selector}
\label{s03-4-1}
Given the current user utterance representation $\mathbf{h}^{X_{\tau}}$ and the knowledge pool representation $\{\mathbf{h}^{K_{\tau,1}},\ldots,$ $\mathbf{h}^{K_{\tau,|\mathcal{K}_{\tau}|}}\}$, the user-initiative selector predicts the probability distribution $P(\mathcal{K}_{\tau} {\mid} user)$ over the knowledge pool $\mathcal{K}_{\tau}$, which is estimated as follows:
\begin{equation}
\label{user_s}
\begin{split}
P(\mathcal{K}_{\tau} {\mid} user)&={}\operatorname{Softmax}(\mathbf{Q}_{user}\mathbf{K}_{user}^\top) \in \mathbb{R}^{1 \times |\mathcal{K}_{\tau}|} \\
\mathbf{Q}_{user}&={}\operatorname{MLP}(\mathbf{h}^{X_{\tau}}) \in \mathbb{R}^{1\times d} \\
\mathbf{K}_{user}&={}\operatorname{MLP}([\mathbf{h}^{K_{\tau,1}};\ldots;\mathbf{h}^{K_{\tau,|\mathcal{K}_{\tau}|}}]) \in \mathbb{R}^{|\mathcal{K}_{\tau}| \times d}
\end{split}
\end{equation}
where $\operatorname{MLP}(\cdot)=\cdot\mathbf{W}+\mathbf{b}$ is a \ac{MLP}, and $[\cdot;\cdot]$ denotes the vector concatenation operation. 

\subsubsection{System-initiative selector}
\label{s03-4-2}
Given the previously selected knowledge representation $\smash{\{\mathbf{h}^{K_{i,sel}}\}^{\tau-1}_{i=1}}$and the knowledge pool representation $\{\mathbf{h}^{K_{\tau,1}},\ldots,\mathbf{h}^{K_{\tau,|\mathcal{K}_{\tau}|}}\}$, the system-initiative selector predicts the probability distribution $P(\mathcal{K}_{\tau} {\mid} sys)$ over the knowledge pool $\mathcal{K}_{\tau}$, which is estimated as follow:
\begin{equation}
\label{sys_s}
\begin{split}
P(\mathcal{K}_{\tau} {\mid} sys)&=\operatorname{Softmax}(\mathbf{Q}_{sys}\mathbf{K}_{sys}^\top) \in \mathbb{R}^{1 \times |\mathcal{K}_{\tau}|} \\
\mathbf{Q}_{sys}&={}\operatorname{MLP}(\mathbf{h}^{K_{\tau-1,sel}}_{trans}) \in \mathbb{R}^{1 \times d} \\
\mathbf{K}_{sys}&={}\operatorname{MLP}([\mathbf{h}^{K_{\tau,1}};\ldots;\mathbf{h}^{K_{\tau,|\mathcal{K}_{\tau}|}}]) \in \mathbb{R}^{|\mathcal{K}_{\tau}| \times d}\\
[\mathbf{h}^{K_{1,sel}}_{trans};\ldots;&\mathbf{h}^{K_{\tau-1,sel}}_{trans}]={}\\
\operatorname{TransformerE}(&[\mathbf{h}^{K_{1,sel}};\ldots;\mathbf{h}^{K_{\tau-1,sel}}])\in \mathbb{R}^{(\tau-1) \times d}, 
\end{split}
\end{equation}
where $\operatorname{TransformerE}$ is a stack of transformer encoder blocks~\citep{vaswani2017attention}; it first adds positional embeddings to inputs, where each turn has its own distinct positional embedding.
We use a left-to-right self-attention mask in these blocks, where every position can only attend to previous positions, e.g. $K_{\tau-2,sel}$ cannot attend to $K_{\tau-1,sel}$.
%Besides, if $\tau$=1, a zero vector is fed into $\operatorname{TransformerE}$.

\subsubsection{Initiative discriminator}
\label{s03-4-3}
Given the current user utterance representation $\mathbf{h}^{X_{\tau}}$ and the previously selected knowledge representations $\smash{\{\mathbf{h}^{K_{i,sel}}\}^{\tau-1}_{i=1}}$, the initiative discriminator predicts the probability of user-initiative \ac{KS} $P(u_\tau)$ at turn $\tau$, which is estimated as follow:
\begin{equation}
\label{id}
\begin{split}
P(u_\tau)&=\operatorname{Sigmoid}(\psi(u_\tau)) \in \mathbb{R}^{1 \times 1}\\
\psi(u_\tau)&=\operatorname{MLP}([\mathbf{h}^{K_{\tau-1,sel}}_{trans};\mathbf{h}^{X_{\tau}}]) \in \mathbb{R}^{1 \times 1}\\ 
[\mathbf{h}^{K_{1,sel}}_{trans};\ldots;&\mathbf{h}^{K_{\tau-1,sel}}_{trans}]={}\\
\operatorname{TransformerE}(&[\mathbf{h}^{K_{1,sel}};\ldots;\mathbf{h}^{K_{\tau-1,sel}}]) \in \mathbb{R}^{(\tau-1) \times d},
\end{split}
\end{equation}
where $\operatorname{TransformerE}$ here has the same setting as the one in Eq.~\ref{sys_s}, but they do not share parameters.
Accordingly, $(1-P(u_\tau))$ is regarded as the probability of system-initiative \ac{KS}.
Given above results, we get the final distribution $P(\mathcal{K}_{\tau})=P(u_\tau)P(\mathcal{K}_{\tau} {\mid} user)+(1-P(u_\tau))P(\mathcal{K}_{\tau} {\mid} sys)$, from which we select the piece of knowledge ${K_{\tau,sel}}$ with the highest probability.

\begin{figure}[t]
\centering
\includegraphics[width=1\columnwidth]{figs/figure_3.pdf}
\caption{Two tasks of \ac{ISLe}}
\label{f3}
\end{figure}

\subsection{Decoding layer}
The concatenated representations of the current user utterance and the selected knowledge \smash{$\mathbf{H}^{XK_\tau}=[\mathbf{H}^{X_\tau};\mathbf{H}^{K_{\tau,sel}}]$}$\in \mathbb{R}^{|XK_\tau| \times d}$ are fed into a transformer decoder~\citep{vaswani2017attention} with a copying mechanism~\citep{gu2016incorporating, see2017get} to generate $Y_{\tau}$.
Note that during training, $\mathbf{H}^{K_{\tau,sel}}$ would be $\mathbf{H}^{K_{\tau,*}}$~($K_{\tau,*}$ is the ground-truth selected knowledge). 
Concretely, the probability of generating $y_{\tau,t}$ at the timestamp $t$ is modeled as:
\begin{equation}
%\label{final_distribution}
\begin{split}
P(y_{\tau,t})=(1-P(c))P_{vocab}(y_{\tau,t})+P(c)\sum_{i:xk_{\tau,i}=y_{\tau,t}}\alpha_{\tau,t,i},
\end{split}
\end{equation}
where $P_{vocab}(y_{\tau,t})$ is the probability of generating $y_{\tau,t}$ from a predefined vocabulary $V$:
\begin{equation}
\label{g}
\begin{split}
P_{vocab}(y_{\tau,t})=&\operatorname{Softmax}(\operatorname{MLP}(\mathbf{h}^{dec_{\tau,t}}_{trans}))\in \mathbb{R}^{1 \times|V|}\\
\mathbf{h}^{dec_{\tau,t}}_{trans}=& \operatorname{TransformerD}(\mathbf{emb}(y_{\tau,<t}),\mathbf{H}^{XK_\tau}) \in \mathbb{R}^{1 \times d},
\end{split}
\end{equation} 
where $\operatorname{TransformerD}$ is a stack of transformer decoder blocks~\cite{vaswani2017attention} and $\mathbf{emb}(y_{\tau,<t})$ denotes the embedding of $y_{\tau,<t}$.

$\sum_{i:xk_{\tau,i}=y_{\tau,t}}\alpha_{\tau,t,i}$ is the probability of copying $y_{\tau,t}$ from the concatenated sequence of the current user utterance and the selected knowledge $XK_\tau=[{X_\tau};{K_{\tau,sel}}]$.
$xk_{\tau,i}$ is the $i$-th token in $XK_\tau$ and $\alpha_{\tau,t}$ is the attention distribution over $XK_\tau$ with $\smash{\mathbf{h}^{dec_{\tau,t}}_{trans}}$ attending to $\mathbf{H}^{XK_\tau}$~(see~Eq.~\ref{c}).

$P(c)$ is used as a soft switch to choose between generating from the vocabulary $V$ and copying from $XK_\tau$, which is estimated as follows:
\begin{equation}
\label{final_distribution}
\begin{split}
P(c)=\operatorname{Sigmoid}(\operatorname{MLP}([\mathbf{h}^{dec_{\tau,t}}_{trans};\mathbf{c}_{\tau,t}])\in \mathbb{R}^{1 \times 1},
\end{split}
\end{equation}
where $\mathbf{c}_{\tau,t}$ is the attention vector derived from $\mathbf{h}^{dec_{\tau,t}}_{trans}$ attending to \smash{$\mathbf{H}^{XK_\tau}$}, which is calculated as follows:
\begin{equation}
\label{c}
\begin{split}
\mathbf{c}_{\tau,t}&={}\alpha_{\tau,t}\mathbf{H}^{XK_\tau} \in \mathbb{R}^{1 \times d}\\
\alpha_{\tau,t}&={}\operatorname{Softmax}(\mathbf{Q}_{c}\mathbf{K}_{c}^\top) \in \mathbb{R}^{1 \times |XK_{\tau}|} \\
\mathbf{Q}_{c}&={}\operatorname{MLP}(\mathbf{h}^{dec_{\tau,t}}_{trans}) \in \mathbb{R}^{1 \times d},\
\mathbf{K}_{c}={}\operatorname{MLP}(\mathbf{H}^{XK_\tau}) \in \mathbb{R}^{|XK_\tau| \times d}.
\end{split}
\end{equation}


\subsection{Initiative-Aware Self-Supervised Learning}
\label{s03-6}
As depicted in Fig.~\ref{f3}, \ac{ISLe} is devised to supervise \ac{MIKe} to discriminate the initiative type of \ac{KS} via two tasks, \emph{locating missing knowledge} and \emph{learning with pseudo initiative labels}.

\subsubsection{Locating missing knowledge}
Given all pieces of ground-truth chosen knowledge in a conversation $\{K_{1,sel},\ldots,K_{|\mathcal{C}|,sel}\}$, we first corrupt it via randomly deleting one piece of knowledge at one of conversation turns (e.g., deleting the piece of knowledge $K_{\tau=m,sel}$ at the $m$-{th} turn, $1<m<|\mathcal{C}|$), and then we introduce a \emph{teacher initiative discriminator} to learn to locate the missing knowledge $K_{m,sel}$ (detecting the knowledge $K_{m+1,sel}$ that is closely after the missing knowledge $K_{m,sel}$), which is estimated as follows:  
\begin{equation}
\label{loc}
\begin{split}
[&\ldots;P(u^{tea}_{m-1});P(u^{tea}_{m+1});\ldots]=\\
&\operatorname{Sigmoid}([\ldots;\psi(u^{tea}_{m-1});\psi(u^{tea}_{m+1});\ldots]) \in \mathbb{R}^{1 \times (|\mathcal{C}|-1)} \\
[&\ldots;\psi(u^{tea}_{m-1});\psi(u^{tea}_{m+1});\ldots]=\\
&\operatorname{MLP}([\ldots;\mathbf{h}^{K_{m-1,sel}}_{trans};\mathbf{h}^{K_{m+1,sel}}_{trans};\ldots]) \in \mathbb{R}^{1 \times (|\mathcal{C}|-1)} \\
[&\ldots;\mathbf{h}^{K_{m-1,sel}}_{trans};\mathbf{h}^{K_{m+1,sel}}_{trans};\ldots]={}\\
&\operatorname{TransformerE}([\ldots;\mathbf{h}^{K_{m-1,sel}};\mathbf{h}^{K_{m+1,sel}};\ldots])\in \mathbb{R}^{1 \times (|\mathcal{C}|-1)},
\end{split}
\end{equation}
where $\operatorname{TransformerE}$ here has the same settings as in Eq.~\ref{sys_s}, but they do not share parameters.
$[\ldots;P(u^{tea}_{m-1});P(u^{tea}_{m+1});\ldots]$ are the probabilities of being the knowledge closely after the missing knowledge, where we want that $P(u^{tea}_{m+1})$ equals 1 and others equal 0, meaning that the knowledge at the $m$-{th} turn is missing and the knowledge shift between $K_{m-1,sel}$ and $K_{m+1,sel}$ is unsmooth.
Therefore, the objective function is defined as: 
\begin{equation}
\label{primary_loss}
\begin{split}
&\mathcal{L}_{loc}(\theta)={}\\
&\quad-\frac{1}{|\mathcal{C}|}\sum_{\tau=1}^{|\mathcal{C}|}
I(\tau) \log P(u^{tea}_{\tau})+(1{-}I(\tau))  \log (1-P(u^{tea}_{\tau})),
\end{split}
\end{equation}
where $\tau \neq m $ and $I(\tau)$ is an indicator function that equals 1 if $\tau=m+1$ and 0 otherwise. 

\subsubsection{Learning with pseudo initiative labels}
We regard the initiative discriminator defined in \S\ref{s03-4-3} as \emph{student initiative discriminator}.
At turn $\tau$, given the previously and currently selected knowledge $\{K_{1,sel},\ldots,K_{\tau,sel}\}$, the teacher initiative discriminator predicts $P(u^{tea}_{\tau})$, the probability that $K_{\tau,sel}$ is the knowledge closely after the missing knowledge, which is approximately equivalent to the probability of user-initiative \ac{KS} at turn $\tau$ based on our insights: 
\begin{equation}
\label{mse}
\begin{split}
P(u^{tea}_\tau)&={}\operatorname{Sigmoid}(\psi(u^{tea}_\tau)) \in \mathbb{R}^{1 \times 1} \\
\psi(u^{tea}_\tau)&={}\operatorname{MLP}(\mathbf{h}^{K_{\tau,sel}}_{trans}) \in \mathbb{R}^{1 \times 1} \\
[\mathbf{h}^{K_{1,sel}}_{trans};\ldots;\mathbf{h}^{K_{\tau,sel}}_{trans}]&={}\\
\operatorname{TransformerE}(&[\mathbf{h}^{K_{1,sel}};\ldots;\mathbf{h}^{K_{\tau,sel}}]) \in\mathbb{R}^{\tau \times d},
\end{split}
\end{equation}
%and the knowledge shift between $K_{\tau-1,sel}$ and $K_{\tau,sel}$ is unsmooth
where $P(u^{tea}_{\tau})$ is also regarded as the pseudo initiative label.
The student initiative discriminator learns with it via an \ac{MSE} loss:
\begin{equation}
\label{mse_loss}
\begin{split}
\mathcal{L}_{mse}(\theta)=
-\frac{1}{|\mathcal{C}|}\sum_{\tau=1}^{|\mathcal{C}|}
(P(u_{\tau})-P(u^{tea}_{\tau}))^2,
\end{split}
\end{equation}
where $P(u_{\tau})$ is the user-initiative \ac{KS} probability predicted by the student initiative discriminator (see~Eq.~\ref{id}).
Note that only the student initiative discriminator would execute during inference.

\subsection{Final learning objective}
Given a conversation $\mathcal{C}=\{(X_\tau,Y_\tau)\}^{|\mathcal{C}|}_{\tau=1}$ with $|\mathcal{C}|$ turns, \ac{MIKe} is optimised in a multi-task learning manner and the final objective function is defined as:
\begin{equation}
\label{final_loss}
\begin{split}
\mathcal{L}(\theta)&=\mathcal{L}_{primary}(\theta)+\lambda{L}_{ISLe}(\theta)\\
\mathcal{L}_{ISLe}(\theta)&=\mathcal{L}_{loc}(\theta)+\mathcal{L}_{mse}(\theta)\\
\mathcal{L}_{primary}(\theta)&=\mathcal{L}_{ks}(\theta)+\mathcal{L}_{g}(\theta),
\end{split}
\end{equation}
where $\theta$ are all the parameters of \ac{MIKe} and 
$\lambda$ is a hyper-parameter as a trade-off between the
objectives of learning primary tasks~(\ac{KS} and response generation) and \ac{ISLe}.
And $\mathcal{L}_{ks}(\theta)$ and $\mathcal{L}_{g}(\theta)$ are the learning objective functions for \ac{KS} and response generation, separately, which are defined as:
\begin{equation}
\label{ks_and_g}
\begin{split}
\mbox{}\hspace*{-1mm}
\mathcal{L}_{ks}(\theta)&=
-\frac{1}{|\mathcal{C}|}\sum_{\tau=1}^{|\mathcal{C}|}
\log P(K_{\tau,*}) \\
\mathcal{L}_{g}(\theta)&=
-\frac{1}{|\mathcal{C}|}\sum_{\tau=1}^{|\mathcal{C}|}
\sum_{t=1}^{|Y_{\tau}|}\log P(y_{\tau,t}\mid y_{\tau,<t}, X_\tau, K_{\tau,*}),
\hspace*{-1mm}\mbox{}
\hspace*{-1mm}\mbox{}
\end{split}
\end{equation}
where $*$ refers to the index of the ground-truth selected knowledge in the knowledge pool $\mathcal{K}_{\tau}$.
