% !TEX root =  ../Main.tex

\section{Introduction}
Open-domain conversational agents~\citep{lei2018sequicity,jin2018explicit,lei2020estimation,huang2020challenges} are mainly based on the seq2seq framework~\citep{sutskever2014sequence}, and they usually condition the response generation only on the conversation context, leading them to generate uninformative responses~\cite{li2016diversity}.
%are designed to converse with users informatively, coherently and engagingly.
%When  such systems are able to generate fluent responses, but they 
\Acfp{KGC} mitigate this problem by conditioning the response generation on external knowledge~\citep{ghazvininejad2018knowledge,dinan2018wizard}.
Because external knowledge contains information that may be redundant or irrelevant to current conversation, \acfi{KS}, that is, choosing the appropriate knowledge to be incorporated in the next response, is a vital part in \ac{KGC}~\citep{kim2020sequential,chuanmeng2020dukenet}.

 \begin{figure}[t]
 \centering
 \includegraphics[width=1\columnwidth]{figs/figure_1.pdf}
 \caption{An example of mixed-initiative \acl{KS} from the \acf{WoW} dataset~\citep{dinan2018wizard}.}
 \label{f1}
\end{figure}

In a conversation, \emph{initiative} is the ability to drive the direction of the conversation~\citep{radlinski2017theoretical}.
Mixed initiative is an intrinsic feature of human-machine conversations~\citep{vakulenko2020analysis,radlinski2017theoretical,walker1990mixed}, where the user and system can both take the initiative in suggesting new conversational directions by introducing new topics, asking questions, and so on.
\Ac{KS} also has the potential for mixed-initiative, i.e., the direction of \ac{KS} can be driven by the user (user-initiative \ac{KS}) or by the system itself (system-initiative \ac{KS}).
For user-initiative \ac{KS}, the system usually selects knowledge according to the current user utterance that contains new topics or questions posed by the user.
As depicted in Fig.~\ref{f1}, at the first turn in the conversation, the current user utterance ``\textit{I love Coca-Cola. How about you?}'' contains a question posed by the user, based on which the system chooses a piece of knowledge about Coca-Cola. 
A similar case can be found at the third turn. 
For system-initiative \ac{KS}, without user's push, the system usually selects knowledge according to the previously selected knowledge.
As shown in Fig.~\ref{f1}, at the second turn, the current user utterance ``\textit{I prefer it over Pepsi.}'' does not contain information suggesting a new conversational direction. 
In this case, the system selects a new piece of knowledge about the history of Coca-Cola based on the previously selected knowledge about the basic information about Coca-Cola.

No previous studies have considered the mixed-initiative nature of \ac{KS}.
As a result, previous studies treat the roles of the current user utterance and the previously selected knowledge equally, regardless of the different roles they play for different types of \ac{KS}. 
We hypothesize that this omission may introduce redundant information and lead to inferior performance of \ac{KS}.
However, modeling mixed-initiative \ac{KS} is challenging since there is no manual label indicating initiative in \ac{KGC} training sets.
Heuristic methods are far from enough to discriminate initiative effectively. 
%For example, just regarding the current \ac{KS} as user-initiative if the current user utterance contains a question mark or question word.
As depicted in Fig.~\ref{f1}, at the third turn in the conversation, the current \ac{KS} is user-initiative but the current user utterance only contains an implicit information need, which is hard to capture using heuristics.

To tackle the above issues, we propose a \acfi{MIKe} for \ac{KGC}, that explicitly distinguishes between user-initiative and system-initiative \ac{KS}.
Specifically, we not only introduce two knowledge selectors to model user-initiative and system-initiative, but also design an \textit{initiative discriminator} to discriminate the initiative type of \ac{KS} at each turn in a conversation.
To overcome the challenge of absent manual labels for indicating initiative, we devise an \acf{ISLe} scheme to make \ac{MIKe} learn to discriminate the initiative types of \ac{KS}, which is based on two insights found in data: 
\begin{enumerate}[leftmargin=*,nosep]
\item
If there is an unsmooth knowledge shift at the current conversation turn (i.e., the knowledge previously selected and the knowledge currently selected cannot be directly connected naturally), the current \ac{KS} tends to be user-initiative. 
Conversely, the \ac{KS} tends to be system-initiative.
As depicted in Fig.~\ref{f1}, the knowledge shift at the third turn is unsmooth (i.e., knowledge about the history of Coca-Cola and Red Bull cannot be directly connected naturally), and the current \ac{KS} is user-initiative.
The opposite situation can be found in the second turn.
\item 
If a piece of knowledge selected at one turn is deleted, the knowledge shift between the knowledge closely before and after the missing knowledge tends to be unsmooth.
As depicted in Fig.~\ref{f1}, if the knowledge selected at the second turn is deleted, the shift between the knowledge selected at the first and third turn is also unsmooth.
\end{enumerate}

\noindent
Building on these intuitions, we hypothesize that learning to locate missing knowledge (detecting the knowledge closely before and after the missing knowledge) is approximately equivalent to learning to detect unsmooth knowledge shifts and detect the user-initiative \ac{KS}.
Thus, \ac{ISLe} supervises \ac{MIKe} via a self-supervised task, \textit{locating missing knowledge}, i.e., given all pieces of ground-truth chosen knowledge in a conversation, a piece of knowledge at one of turns is randomly deleted and then the initiative discriminator is required to detect the knowledge closely after the missing knowledge (the knowledge closely before the missing knowledge could be known accordingly).
Through the learning, at each turn, given the previously and currently chosen knowledge, if the initiative discriminator identifies the currently chosen knowledge as the knowledge closely after the missing knowledge, the knowledge shift between the previously and currently chosen knowledge would be unsmooth and thus the current \ac{KS} would be user-initiative.
Otherwise, the current \ac{KS} would be system-initiative.

At each conversation turn, the knowledge currently selected is the target to predict and cannot be fetched during inference.
Therefore, we further distinguish the initiative discriminator as a \textit{teacher initiative discriminator} and a \textit{student initiative discriminator} and upgrade \ac{ISLe} to two tasks:
\begin{enumerate*}
\item \emph{locating missing knowledge}; the teacher is required to learn to locate the missing knowledge;
\item \emph{learning with pseudo initiative labels}; at each turn, the teacher is first executed to generate the pseudo-label indicating the initiative type of \ac{KS}, and then the student is required to learn the pseudo initiative label estimated by the teacher.
\end{enumerate*}
During inference, we only execute the student discriminator.

Experiments on the \ac{WoW}~\citep{dinan2018wizard} and Holl-E~\citep{moghe2018towards} datasets indicate that \ac{MIKe} can choose more appropriate knowledge, and generate more informative and engaging responses.
\ac{ISLe} helps \ac{MIKe} to effectively discriminate the initiative type for \ac{KS}.

The contributions of this paper can be summarized as follows:
 \begin{itemize}[leftmargin=*,nosep]
 \item We propose \acfi{MIKe} for KGC, which explicitly distinguishes between user-initiative and system-initiative \ac{KS} at each conversation turn so as to improve the performance of \ac{KS}.
 \item We devise \acfi{ISLe}, which helps \ac{MIKe} discriminate \ac{KS} initiative types via an approximately equivalent self-supervised task, \textit{locating missing knowledge}.
 \item We conduct automatic and human evaluations on two benchmark datasets, which reveals that \ac{MIKe} can choose more appropriate knowledge and generate more informative and engaging responses, significantly outperforming state-of-the-art methods in terms of both automatic and human evaluation.
 \end{itemize}
