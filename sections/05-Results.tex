% !TEX root =  ../Main.tex

\section{Experimental results}
\subsection{Automatic evaluation}
\label{s05-1}

Tables~\ref{wizard} and \ref{holl_e} show the results of all approaches on the \ac{WoW} and Holl-E datasets, respectively.
Overall, \ac{MIKe} achieves new state-of-the-art performance on all metrics on both datasets.
There are two main observations from the results.

First, \ac{MIKe} distinctly outperforms other baselines in terms of \ac{KS} (see R@1) on both datasets.
Concretely, the R@1 percentage of \ac{MIKe} exceeds the percentage of the strongest baseline SKT+PIPM+KDBTS by 1.01\%--1.27\% on the \ac{WoW} dataset and by 1.06\%--1.08\% on the Holl-E dataset.
The gains indicate that explicitly distinguishing the initiative type of \ac{KS} makes \ac{MIKe} focus on the more important one for the current \ac{KS} from two parts (the current user utterance or previously selected knowledge), leading to better performance of \ac{KS}.
It is worth noting that \ac{MIKe} has a stronger ability of generalization than the baselines.
Specifically, the R@1 gap between \ac{MIKe} and the strongest baseline SKT+PIPM+KDBTS is 1.01\% on Test Seen (in-domain) and 1.27\% on Test Unseen (out-of-domain).
The self-supervised task in \ac{ISLe} exploits more natural and universal patterns contained in \ac{KGC} data compared to other baselines, making \ac{MIKe} perform well when fed with out-of-domain data.

Second, \ac{MIKe} significantly outperforms other baselines in terms of response generation (see BLEU-4, METEOR, ROUGE-1/2/L) on both datasets.
Note that \ac{MIKe} has almost the same decoder as these strong baselines, indicating that the better \ac{KS} performance of \ac{MIKe} further improves the quality of generated responses.

\subsection{Human evaluation}
\label{s05-2}

Table~\ref{human_evaluation} shows the results of a comparison between \ac{MIKe} and the three most competitive baselines (SKT+PIPM+KDBTS, DukeNet, \ac{DiffKS}+BERT) on the more challenging \ac{WoW} dataset; qualitatively similar results were observed on the Holl-E dataset.

Overall, \ac{MIKe} achieves the best performance on all metrics on Test Seen and Test Unseen.
\ac{MIKe} outperforms the baselines in terms of Appropriateness (evaluating \ac{KS}), especially having more obvious advantages over the baselines on Test Unseen.
For example, the win ratio of \ac{MIKe} versus the most competitive baseline SKT+PIPM+KDBTS is 25\% on Test Seen and 29\% on Test Unseen, which is consistent with the observations of our automatic evaluation. 
Surprisingly, in spite of having almost the same decoder as these baselines, \ac{MIKe} still has a clear advantage over these baselines in terms of  Informativeness and Engagingness (evaluating response generation).
The knowledge selected by \ac{MIKe} is more appropriate and thus the corresponding generated responses contain more coherent and useful information.

\begin{table*}[th]
\centering
\setlength\tabcolsep{2.5pt}
\caption{
Human evaluation results on the \ac{WoW} dataset.
}
\label{human_evaluation}
%\resizebox{1\textwidth}{!}{
\begin{tabular}{lccccccccc ccccccccc}
\toprule
\multirow{3}{*}{\textbf{Methods}} 
& \multicolumn{9}{c}{\textbf{Test Seen (\%)}} & \multicolumn{9}{c}{\textbf{Test Unseen (\%)}} \\ 
 \cmidrule(lr){2-10}   \cmidrule(lr){11-19} 
 & \multicolumn{3}{c}{Appropriateness} & \multicolumn{3}{c}{Informativeness} & \multicolumn{3}{c}{Engagingness} & \multicolumn{3}{c}{Appropriateness} & \multicolumn{3}{c}{Informativeness} & \multicolumn{3}{c}{Engagingness} \\
 \cmidrule(r){2-4} \cmidrule(r){5-7} \cmidrule(r){8-10} \cmidrule(r){11-13} \cmidrule(r){14-16} \cmidrule(r){17-19}
 & \multicolumn{1}{c}{Win} & \multicolumn{1}{c}{Tie} & \multicolumn{1}{c}{Lose} & \multicolumn{1}{c}{Win} & \multicolumn{1}{c}{Tie} & \multicolumn{1}{c}{Lose} & \multicolumn{1}{c}{Win} & \multicolumn{1}{c}{Tie} & \multicolumn{1}{c}{Lose} & \multicolumn{1}{c}{Win} & \multicolumn{1}{c}{Tie} & \multicolumn{1}{c}{Lose} & \multicolumn{1}{c}{Win} & \multicolumn{1}{c}{Tie} & \multicolumn{1}{c}{Lose} & \multicolumn{1}{c}{Win} & \multicolumn{1}{c}{Tie} & \multicolumn{1}{c}{Lose} \\ \cmidrule(r){1-19}
MIKe vs DiffKS + BERT & 32  & 59   & 9   & 18 & 76  & 6    & 26 & 62  & 12    & 27  & 67  & 6    & 19   & 77  & 4     & 24 & 64  & 12    \\
MIKe vs DukeNet & 27  & 64   & 9    & 18  & 75  & 7     & 22 & 65  & 13     & 30    & 66  & 4     & 18   & 74  & 8  & 24  & 61  & 15    \\
MIKe vs SKT+PIPM+KDBTS  & 25 & 67 & 8    & 17  & 78  & 5      & 20 & 69  & 11    & 29 & 66 & 5    & 19  & 76  & 5    & 25 & 62 & 13  \\
\bottomrule
\end{tabular}
%}
\end{table*}

\begin{table*}[ht]
\centering
\setlength\tabcolsep{1.1pt}
\caption{
Ablation study on the \ac{WoW} dataset.
-\ac{ISLe} denotes removing initiative-aware self-supervised learning.
-ID denotes removing initiative discriminator.
-SIS and -UIS denote removing system-initiative selector and user-initiative selector.
}
\label{ablation_result}
%\resizebox{1.0\textwidth}{!}{
\begin{tabular}{l cccccc cccccc}
\toprule
\multirow{2}{*}{\textbf{Methods}} & \multicolumn{6}{c}{\textbf{Test Seen (\%)}} & \multicolumn{6}{c}{\textbf{Test Unseen (\%)}}\\
\cmidrule(lr){2-7} \cmidrule(lr){8-13}  
& BLEU-4     & METEOR   & ROUGE-1   & ROUGE-2 & ROUGE-L & R@1  
& BLEU-4   & METEOR & ROUGE-1  & ROUGE-2   & ROUGE-L  & R@1\\ 
\midrule
MIKe~(ours) & \textbf{2.78}   & \textbf{17.76}  & \textbf{25.40}  & \textbf{7.11} & \textbf{18.78}  & \textbf{28.41}    & \textbf{2.00} & \textbf{15.64}  & \textbf{23.78} & \textbf{5.61}  & \textbf{17.41} & \textbf{21.47} \\
\ac{MIKe}-\ac{ISLe} & 2.63   & 17.22   & 25.15    & 6.97  & 18.67    & 27.52    & 1.67  & 15.38  & 23.42   & 5.28  & 17.04  & 20.44   \\
\ac{MIKe}-\ac{ISLe}-ID & 2.48  & 17.28   & 24.90   & 6.64 & 18.24 & 26.58   & 1.46  & 14.70  & 22.87  & 5.16  & 16.36  & 19.35  \\
\ac{MIKe}-\ac{ISLe}-ID-UIS & 1.70   & 15.88   & 24.37   & 5.17  & 17.33   & 23.95    & 0.89  & 13.68  & 22.17    & 4.09 & 15.98  & 16.67    \\
\ac{MIKe}-\ac{ISLe}-ID-SIS & 1.68   & 15.76   & 24.33   & 5.08  & 17.21   & 23.88    & 0.87 & 13.44   & 22.01   & 3.88 & 15.79   & 15.99    \\
\bottomrule
\end{tabular}
\end{table*}

\begin{table}[ht]
\centering
%\setlength\tabcolsep{1.1pt}
\caption{
Statistics about the manual annotation of initiative type of \ac{KS} on the \ac{WoW} dataset.
}
\label{statistics}
%\resizebox{0.5\textwidth}{!}{
\begin{tabular}{l c c}
\toprule
\multirow{1}{*}{\textbf{Initiative type}} & \multicolumn{1}{c}{\textbf{Test Seen (\%)}} & \multicolumn{1}{c}{\textbf{Test Unseen (\%)}}\\
\midrule
User-initiative \ac{KS} & 47.80  & 48.70 \\
System-initiative \ac{KS}   & 52.20  & 51.30 \\
\midrule
\end{tabular}
%}
\end{table}




