% !TEX root =  ../Main.tex

\section{Experimental setup}
To assess the performance of \ac{MIKe} we compare it against a number of state-of-the-art baselines. 
We also analyze the contribution of some of the ingredients that make up \ac{MIKe}, in particular, the initiative discriminator and \ac{ISLe}.

\subsection{Datasets}
\label{s04-1}
Following~\citep{kim2020sequential,chuanmeng2020dukenet, zheng2020difference, chen2020bridging}, we evaluate our model on two \ac{KGC} datasets, \acf{WoW}~\citep{dinan2018wizard} and Holl-E~\citep{moghe2018towards}.
Both contain the ground-truth labels for \ac{KS}~(only one piece of knowledge is true for \ac{KS} per turn).
We split the data into training, validation and test as per the original papers.

\emph{~\ac{WoW}} is a \ac{KGC} dataset based on \textit{piece-based unstructured knowledge}, i.e., each conversation is given some separate pieces of knowledge.
In this dataset, a piece of knowledge is defined as a knowledge sentence.
Each conversation is conducted between a wizard who can retrieve knowledge sentences from Wikipedia and then choose one to produce a response and an apprentice who is active in talking with the wizard.
It contains 18,430/1,948/1,933 conversations for training/validation/test. 
The test set is split into two subsets, Test Seen~(in-domain) and Test Unseen~(out-of-domain):
the former contains conversations on topics appearing in the training set, while the latter contains conversations on new topics.
There are around 67 pieces of knowledge on average in a knowledge pool.

\emph{Holl-E} is originally a \ac{KGC} dataset based on \textit{document-based unstructured knowledge}.
\citet{kim2020sequential} have changed it to a version having the same format as \ac{WoW} by splitting the document into separate sentences, and recreated the ground-truth labels for \ac{KS}, so we use the version released by them.
It contains 7,228/930/913 conversations for training/validation/test. 
There are also two versions of the test set: one with a single reference and the other with multiple references~(more than one ground-truth pieces of knowledge and responses for each given conversation context).
There are nearly 60 pieces of knowledge  on average in a knowledge pool.


\subsection{Baselines}
We compare \ac{MIKe} with state-of-the-art \ac{KGC} methods focusing on \textit{explicit \ac{KS}} and leveraging \textit{piece-based unstructured knowledge}.

\begin{itemize}[leftmargin=*,nosep]
\item \textbf{\ac{TMemNet}}~\citep{dinan2018wizard} extends a transformer model with a memory network storing knowledge in an end-to-end manner.
Following~\citep{kim2020sequential,chuanmeng2020dukenet,chen2020bridging}, we replace the original transformer encoder with a BERT encoder, naming it \ac{TMemNet}+BERT.

\item \textbf{\ac{PostKS}}~\citep{Lian2019Learning} uses context and response to jointly predict a posterior knowledge distribution and regards it as pseudo-labels to supervise \ac{KS}.
Following~\citep{kim2020sequential,chuanmeng2020dukenet,chen2020bridging}, we use a BERT encoder for this baseline, naming it \ac{PostKS}+BERT.

\item \textbf{\ac{SKT}}~\citep{kim2020sequential} makes use of the previously selected knowledge and context to jointly facilitate \ac{KS}.
It uses a BERT encoder and incorporates a copying mechanism in decoder~\citep{gu2016incorporating,see2017get}.

\item \textbf{\ac{SKT}}+\textbf{\ac{PIPM}}+\textbf{\ac{KDBTS}}~\citep{chen2020bridging} upgrades \ac{SKT} by adding a \acf{PIPM} and proposing \acf{KDBTS} to improve \ac{KS}.

\item \textbf{\ac{DukeNet}}~\citep{chuanmeng2020dukenet} regards tracking the previously selected knowledge and selecting the current knowledge as dual tasks and use dual learning~\citep{qin2020dual} to supervise them.
It uses a BERT encoder and incorporates a copying mechanism in the decoder.

\item \textbf{\ac{DiffKS}}~\citep{zheng2020difference} utilizes the difference in information between the previously selected knowledge and the current candidate knowledge to improve \ac{KS}.
It uses a GRU~\citep{cho2014learning} encoder, pre-trained GloVe embedding\citep{pennington2014glove} and a copying mechanism in decoder.
For a fair comparison, we replace the GRU encoder and GloVe embedding with a BERT encoder, naming it \ac{DiffKS}+BERT.
\end{itemize}


\subsection{Evaluation metrics}
For automatic evaluation, we evaluate \ac{KS} with Recall@1~(R@1) and evaluate response generation with sentence-level BLEU-4 
~\citep{papineni2002bleu}, METEOR~\citep{denkowski2014meteor}, ROUGE-1/2/L~\citep{lin2004rouge}, which are widely-used in previous studies~\citep{zheng2019enhancing,kim2020sequential,chen2020bridging, zheng2020difference,chuanmeng2020dukenet}.
For the evaluation of multiple references in the Holl-E dataset, we follow \citet{chuanmeng2020dukenet} who evaluate \ac{KS} by regarding the knowledge chosen by the model as correct once it matches any of the ground-truth knowledge, and evaluate response generation by taking the max score between responses generated by models and the multiple ground-truth responses.

We conduct human evaluation on Amazon Mechanical Turk.\footnote{\url{https://www.mturk.com/}}
We first randomly sample 300 examples from each test set, and each of them 
is annotated by three annotators.
Concretely, each annotator is shown an example containing a context, the knowledge pool (at most 10 pieces of knowledge are shown to reduce the workload of the annotators), the pieces of knowledge chosen by \ac{MIKe}, and a baseline (their names are masked out during annotation), as well as the responses generated by the both.
Each annotator then needs to give a preference (ties are allowed) between \ac{MIKe} and a baseline based on three aspects~\citep{chuanmeng2020dukenet}: 
\begin{enumerate*}
\item \emph{appropriateness}, i.e., which chosen knowledge is more appropriate according to the given context; 
\item \emph{informativeness}, i.e., which response looks more informative; and
\item \emph{engagingness}, i.e., which response is better in general.
\end{enumerate*}


\subsection{Implementation details}
For all models, we apply BERT-Base-Uncased~(110M) as the encoder\footnote{\url{https://github.com/huggingface/transformers}}~(hidden size 768), use the BERT vocabulary~(the size is 30,522), set the learning rate to 0.00002, use the Adam optimizer~\citep{kingma2014adam} to optimize parameters, use gradient clipping with a maximum gradient norm of 0.4, train up to 10 epochs, and select the best checkpoints based on performance on the validation set.
For \ac{MIKe}, we set $\lambda$ in Eq.~\ref{final_loss} to 0.5, batch whole conversations rather than individual turns, train our model on one NVIDIA TITAN RTX GPU.

\begin{table*}[ht]
\centering
\setlength\tabcolsep{2pt}
\caption{
Automatic evaluation results on the \ac{WoW} dataset. 
\textbf{Bold face} indicates the best result in terms of the corresponding metric.
Significant improvements over the best baseline results are marked with $^\ast$ (t-test, $p < 0.05$).
}
\label{wizard}

\begin{tabular}{l cccccc cccccc}
\toprule
\multirow{2}{*}{\textbf{Methods}} & \multicolumn{6}{c}{\textbf{Test Seen (\%)}} & \multicolumn{6}{c}{\textbf{Test Unseen (\%)}}\\
\cmidrule(lr){2-7} \cmidrule(lr){8-13}  
& BLEU-4     & METEOR   & ROUGE-1   & ROUGE-2 & ROUGE-L & R@1   
& BLEU-4   & METEOR & ROUGE-1  & ROUGE-2   & ROUGE-L  & R@1\\ 
\midrule
\ac{PostKS} + BERT   & 0.77   & 14.16  & 22.68  & 4.27  & 16.59 & \phantom{2}4.83  & 0.39 & 12.59 & 20.82  & 2.73  & 15.25 & \phantom{1}4.39   \\
\ac{TMemNet} + BERT  & 1.61  & 15.47  & 24.12  & 4.98 & 17.00  & 23.86  & 0.60  & 13.05  & 21.74 & 3.63 & 15.60 & 16.33\\
\ac{SKT}  & 1.76  & 16.04   & 24.61  & 5.24 & 17.61 & 25.36   & 1.05  & 13.74  &22.84  & 4.40  & 16.05  & 18.19  \\ 
DiffKS + BERT & 2.22  & 16.82  & 24.75  & 6.27  & 17.90  & 25.62   & 1.69   &14.69   & 23.62  & 5.05  & 16.82  & 20.11 \\
DukeNet  & 2.43  & 17.09  & 25.17   & 6.81  & 18.52   & 26.38   & 1.68  & 15.06  & 23.34   & 5.29  & 17.06  & 19.57   \\ 
SKT+PIPM+KDBTS  & 2.47   & 17.14  & 25.19   & 7.01 & 18.47 & 27.40    & 1.71  & 14.83   & 23.56 & 5.46  & 17.14  & 20.20 \\

\midrule 
MIKe~(ours) & \textbf{2.78}\rlap{$^\ast$}   & \textbf{17.76}\rlap{$^\ast$}  & \textbf{25.40}  & \textbf{7.11} & \textbf{18.78}\rlap{$^\ast$}  & \textbf{28.41}\rlap{$^\ast$}    & \textbf{2.00}\rlap{$^\ast$}  & \textbf{15.64}\rlap{$^\ast$}  & \textbf{23.78}\rlap{$^\ast$}  & \textbf{5.61}  & \textbf{17.41}\rlap{$^\ast$}  & \textbf{21.47}\rlap{$^\ast$} \\
\bottomrule
\end{tabular}
\end{table*}

\begin{table*}[ht]
\centering
\setlength\tabcolsep{2pt}
\caption{
Automatic evaluation results on the Holl-E dataset. Same conventions as in Table~\ref{wizard}.
}
\label{holl_e}
%\resizebox{1.0\textwidth}{!}{
\begin{tabular}{l cccccc cccccc}
\toprule
\multirow{2}{*}{\textbf{Methods}} & \multicolumn{6}{c}{\textbf{Single golden reference (\%)}} & \multicolumn{6}{c}{\textbf{Multiple golden references (\%)}}\\
\cmidrule(lr){2-7} \cmidrule(lr){8-13}  
& BLEU-4  & METEOR   & ROUGE-1   & ROUGE-2 & ROUGE-L & R@1   
& BLEU-4   & METEOR & ROUGE-1  & ROUGE-2   & ROUGE-L  &R@1 \\ 
\midrule

\ac{PostKS} + BERT   & \phantom{1}6.54  & 19.30   & 28.94  & \phantom{1}9.89 & 22.15   & \phantom{1}3.95    & \phantom{1}8.49 & 23.97  & 32.85  & 13.10  & 26.17  & \phantom{1}6.40 \\
\ac{TMemNet} + BERT  & \phantom{1}8.99   & 24.48  &31.65   &13.24  &25.90  & 28.44     & 12.36 & 28.61 & 35.29  &16.14  & 29.51 & 37.30\\
\ac{SKT}  & 17.81    & 29.41   & 35.28  & 21.74   & 30.06  & 28.99   & 24.69  & 35.78  &41.68   &28.30  &36.24  & 39.05  \\ 
DiffKS + BERT & 19.08   & 30.87    & 36.37   & 22.88 & 31.30 & 29.39   & 26.20   & 37.32  & 42.77 & 29.57   & 37.53  & 38.99 \\
DukeNet  & 19.15   & 30.93  & 36.53 & 23.02  & 31.46 & 30.03  & 26.83  & 37.73  & 43.18 & 30.13 & 38.03 & 40.33 \\
SKT+PIPM+KDBTS  & 20.07   & 31.07   & 36.78  & 24.29 & 31.70 & 30.80 & 27.49  & 37.34   & 43.07 & 30.91   & 37.82   & 40.70 \\

\midrule 
MIKe~(ours) & \textbf{21.14}\rlap{$^\ast$}  & \textbf{32.28}\rlap{$^\ast$}   & \textbf{37.78}   & \textbf{25.31}\rlap{$^\ast$} & \textbf{32.82}\rlap{$^\ast$}  & \textbf{31.86}\rlap{$^\ast$}    & \textbf{28.52}\rlap{$^\ast$}   & \textbf{38.55}\rlap{$^\ast$}    & \textbf{44.06}   & \textbf{31.92}\rlap{$^\ast$}    & \textbf{38.91}\rlap{$^\ast$}   & \textbf{41.78}\rlap{$^\ast$} \\
\bottomrule

\end{tabular}
\end{table*}
